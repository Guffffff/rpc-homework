package com.test.rpc.server;

import org.apache.hadoop.ipc.VersionedProtocol;

public interface MethodInterface extends VersionedProtocol {

    long versionID = 1L;

    String getName(String id);

}
