package com.test.rpc.server;

import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.ipc.ProtocolSignature;

import java.io.IOException;

public class MethodInterfaceImpl implements MethodInterface{

    @Override
    public String getName(String id) {
        if (StringUtils.isNotEmpty(id)){
            switch (id){
                case "20210123456789":
                    return "心心";
            }
        }
        return null;
    }

    @Override
    public long getProtocolVersion(String s, long l) throws IOException {
        return MethodInterface.versionID;
    }

    @Override
    public ProtocolSignature getProtocolSignature(String s, long l, int i) throws IOException {
        return null;
    }

}
