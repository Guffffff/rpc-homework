package com.test.rpc.server;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.ipc.RPC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Server {

    private static final Logger logger = LoggerFactory.getLogger(Server.class);

    public static void main(String[] args){
        RPC.Builder builder = new RPC.Builder(new Configuration());
        builder.setBindAddress("127.0.0.1");
        builder.setPort(12345);

        builder.setProtocol(MethodInterface.class);
        builder.setInstance(new MethodInterfaceImpl());

        RPC.Server server = null;
        try {
            server = builder.build();
            server.start();
        } catch (IOException e) {
            logger.error("server端异常" + e, e);
        }
    }
}
