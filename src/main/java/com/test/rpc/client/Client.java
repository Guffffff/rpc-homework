package com.test.rpc.client;


import com.test.rpc.server.MethodInterface;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.ipc.RPC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Scanner;

public class Client {

    private static Logger logger = LoggerFactory.getLogger(Client.class);

    public static void main(String[] args){
        try{
            MethodInterface proxy = RPC.getProxy(MethodInterface.class,1L, new InetSocketAddress("127.0.0.1", 12345), new Configuration());
            Scanner input = new Scanner(System.in);
            String value = null;
            do{
                System.out.println("请输入：");
                value = input.next();
                System.out.println("返回名称为：" + proxy.getName(value));
            }while (!"exit".equals(value));
        }catch (IOException e){
            logger.error("异常为：" + e.getMessage(), e);
        }
    }

}
